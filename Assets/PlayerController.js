﻿#pragma strict

private var controller : CharacterController;
private var ForZmoveCubeTransform : Transform; 
private var moveVector : Vector3 = Vector3.zero;
private var direction : Vector3 = Vector3.zero;
private var airDirection : Vector3 = Vector3.zero;

public var speed : float = 4;
public var AirTime : float = 0;
public var jumpPower : float = 4;

private var WhetherChild : boolean = false;

private var DashFlag : boolean;
private var PowerMode : boolean;

public var Target : GameObject;
public var DeathZone : GameObject;
public var TofuDead : GameObject;

public var materials:Material[];
private var index:int = 0;
  
function Start () 
{
   controller = GetComponent(CharacterController);
   ForZmoveCubeTransform = GameObject.Find( "ForZmoveCube" ).GetComponent( Transform );
   
  GetComponent.<Renderer>().material = materials[index];
}



function Update () 
{

   var inputX : float = Input.GetAxis("Horizontal");
   var inputY : float = Input.GetAxis("Vertical");
   var inputDirection : Vector3 = Vector3(inputX, 0, inputY);
   moveVector = ( ForZmoveCubeTransform.transform.right.normalized * inputX ) + ( ForZmoveCubeTransform.transform.forward.normalized * inputY );

   if( controller.isGrounded ) 
      {
        moveVector.y = 0;
        direction = moveVector * speed;
        AirTime = 0;
      
      if( inputDirection.magnitude > 0.1 ) 
          {
               transform.rotation = Quaternion.Slerp( transform.rotation, Quaternion.LookRotation( moveVector ), 0.5 );
          }
         
        if( Input.GetButton( "Jump" )) 
          {
               direction.y += jumpPower;
          }
       } 
     
   if( !controller.isGrounded ) 
       {
         AirTime += Time.deltaTime;
       
         airDirection = moveVector * speed * 0.5;
         controller.Move( airDirection * Time.deltaTime );
         airDirection.y += Physics.gravity.y * Time.deltaTime;
         
         if( AirTime >= 10 ){
            DeathZone.SendMessage( "Dead" );
            TofuDead.SendMessage( "StartFadeout" );
            }
       }
    
    controller.Move( direction * Time.deltaTime );
    direction.y += Physics.gravity.y * Time.deltaTime;
                           
                          
                           
                             
    if ((Input.GetButton ("R1Button")) && (DashFlag == false)) 
		{
			DashFlag = true;
			speed = 10;

		} else {
		
			DashFlag = false;

			speed = 4;  
		}   
		
		if ((Input.GetButtonDown ("SankakuButton")) && (PowerMode == true)) 
		{
			PowerMode = false;
			index--;
		
		Target.GetComponent.<Renderer>().material = materials[index];
			pushpower = 10;

		} else if ((Input.GetButtonDown ("SankakuButton")) && (PowerMode == false)) 
		{
			PowerMode = true;
            index++;
            Target.GetComponent.<Renderer>().material = materials[index];
			pushpower = 100;
		}      
		
		               
}
   

  
 


// for elevator enter
function OnTriggerStay ( col : Collider )
{
    if(!( WhetherChild ) && col.gameObject.tag == "PlateHantei")
     {
        this.transform.parent = col.transform.parent;
        WhetherChild = true;
        //Debug.Log ("In");
     }

}


function OnTriggerExit ( col : Collider)
{
     if(col.gameObject.tag == "PlayerHantei")
     {
       this.transform.parent = null;
       WhetherChild = false;
       //Debug.Log ("out");
     }
}



public var pushpower : float = 10;

function OnControllerColliderHit( hit : ControllerColliderHit ) 
{
    if( hit.gameObject.tag == "PushObject" ) 
    {
        hit.collider.attachedRigidbody.
        AddForce( transform.forward * pushpower );
    } 
    
    if( hit.gameObject.tag == "Other" ) 
    {
        hit.collider.attachedRigidbody.
        AddForce( transform.forward * pushpower );
    } 
}





